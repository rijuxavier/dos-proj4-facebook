package main.scala
// Akka Actor system
import akka.actor._
import spray.client.pipelining._
//import spray.http.{HttpRequest, HttpResponse}
import spray.json.DefaultJsonProtocol
import spray.json.AdditionalFormats
import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Success
import spray.json._
import spray.http._
import scala.concurrent.duration._
import spray.httpx.SprayJsonSupport._

package main.scala {

import spray.httpx.SprayJsonSupport

// trait with single function to make a GET request
trait WebClient {
  def get(url: String): Future[String]
}

// implementation of WebClient trait
class sprayClient(implicit system: ActorSystem) extends WebClient {

  // create a function from HttpRequest to a Future of HttpResponse
  val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
  //val pipeline: HttpRequest => Future[??] = sendReceive ~> unmarshal[??]

  // create a function to send a GET request and receive a string response
  def get(url: String): Future[String] = {
    val futureResponse = pipeline(Get(url))
    futureResponse.map(_.entity.asString)
  }
}

object myJson extends DefaultJsonProtocol {
  implicit val userDataJson = jsonFormat4(userDataFormat)
  implicit val postDataJson = jsonFormat4(postDataFormat)
  implicit val pageDataJson = jsonFormat6(pageDataFormat)
}

case class userDataFormat(var userId: Long,
                          var userDname: String,
                          var userFriends: List[Int],
                          var userPostIds: List[Long]
                         )

case class postDataFormat(var postId: Long,
                          var postMessage: String,
                          var postCreatedTime: String,
                          var postToPage: Boolean
                         )

case class pageDataFormat(
                           var pageId: Long,
                           var pageLikes: Int,
                           var pageMembers: List[Long],
                           var pageAbout: String,
                           var pageDescription: String,
                           var pagePost: List[String]
                           //var pageAccessToken: Option[String],
                           //var pageCanPost: Option[Boolean],
                           //var pageCategory: Option[String],
                           //var pageContext: Option[String],
                           //var pageLocation: Option[String],
                           //var pageLink: Option[String],
                           //var pageName: Option[String]
                         )

object Client extends App with SprayJsonSupport with AdditionalFormats{

  implicit val system = ActorSystem("clientNode")
  import myJson._

  // create the client
  val numUsers = 1000
  val numPages = numUsers/2
  val numPosts = numUsers
  val sleepTime = 0
  val sleepTime2 =  0 //numUsers/2
  val myClient = new sprayClient()(system)


//  Build FB
//      val pipeline: HttpRequest => Future[String] = (
//        sendReceive
//          ~> unmarshal[String]
//        )
//      val Req: Future[String] = pipeline(Post("http://127.0.0.1:8080/build"))
//      Req onComplete {
//        case Success(response) => println(response)
//        //case Failure(error) => println("An error has occurred: " + error.getMessage)
//      }

  val simBeginTime = System.currentTimeMillis()

  println("Add "+numUsers+" Users and "+numUsers+" Friend Requests Begin")
  //ADD USERS REQ
  for (i <- 0 until numUsers) {
    Thread.sleep(sleepTime)
    val pipeline: HttpRequest => Future[String] = (
      sendReceive
        ~> unmarshal[String])
    val tempName = "UserName"+i
    val tempUser = new userDataFormat(0,tempName,Nil,Nil)
    //println(tempUser.toJson.prettyPrint)
    val Req: Future[String] = pipeline(Post("http://127.0.0.1:8080/addUser/"+i,
      HttpEntity(ContentTypes.`application/json`, tempUser.toJson.toString)))

    Req onComplete {
      case Success(response) => //println(response)
      //case Failure(error) => println("An error has occurred: " + error.getMessage)
    }
  }
  println("Add "+numUsers+" Users and "+numUsers+"Friend Requests End")


  //ADD PAGE REQ

  println("Add "+numPages+" Pages Begin")
  Thread.sleep(sleepTime2)
  for (i <- 0 until numPages) {

    val tempPageData = new pageDataFormat(0,0,Nil,"This is Page"+i,"This is A Cool Page"+i,Nil)
    //println(tempPostData.toJson.prettyPrint)

    val pipeline: HttpRequest => Future[String] = (
      sendReceive
        ~> unmarshal[String]
      )

    val Req: Future[String] = pipeline(Post("http://127.0.0.1:8080/addPage/"+i,
      HttpEntity(ContentTypes.`application/json`, tempPageData.toJson.toString)))

    Req onComplete {
      case Success(response) => //println(response)
      //case Failure(error) => println("An error has occurred: " + error.getMessage)
    }
  }
  println("Add "+numPages+" Pages End")

  //GET USER DETAILS REQ
  println("Get "+numUsers+" User Details Begin")
  Thread.sleep(sleepTime2)
  for (i <- 0 until numUsers) {
    Thread.sleep(sleepTime)
      val futureResponse = myClient.get("http://127.0.0.1:8080/getUser/"+i)
    // wait for Future to complete`
    futureResponse onComplete {
      case Success(response) => //println("Yes"+response)
      //case Failure(error) => println("An error has occurred: " + error.getMessage)
    }
  }
  println("Get "+numUsers+" User Details End")


  //GET USER FRIENDS LIST
  println("Get "+numUsers+" User Friends List Begin")
  Thread.sleep(sleepTime2)
  for (i <- 0 until numUsers) {
    Thread.sleep(sleepTime)
    val futureResponse = myClient.get("http://127.0.0.1:8080/getFriends/"+i)
    // wait for Future to complete`
    futureResponse onComplete {
      case Success(response) => //println("Friendlist:"+response+"for User:"+i)
      //case Failure(error) => println("An error has occurred: " + error.getMessage)
    }
  }
  println("Get "+numUsers+" User Friends List End")



  println("Add "+numPosts+" Posts Begin")
  Thread.sleep(sleepTime2)
  //ADD POST REQ
  for (i <- 0 until numPosts) {
    Thread.sleep(sleepTime)
    val tempPost = "Hello from User"+i
    val tempPostData = new postDataFormat(0,tempPost,"",false)
    //println(tempPostData.toJson.prettyPrint)

    val pipeline: HttpRequest => Future[String] = (
      sendReceive
        ~> unmarshal[String]
      )

    //val Req: Future[String] = pipeline(Post("http://127.0.0.1:8080/addPost/"+tempPost+"/"+i))
    val Req: Future[String] = pipeline(Post("http://127.0.0.1:8080/addPost/"+i,
      HttpEntity(ContentTypes.`application/json`, tempPostData  .toJson.toString)))

    Req onComplete {
      case Success(response) => //println(response)
      //case Failure(error) => println("An error has occurred: " + error.getMessage)
    }
  }
  println("Add "+numPosts+" Posts End")


  //GET PAGE REQ
  println("Get "+numPages+" Pages Begin")
  Thread.sleep(sleepTime2)

  for (i <- 0 until numPages) {
    Thread.sleep(sleepTime)
    val futureResponse = myClient.get("http://127.0.0.1:8080/getPage/"+i)
    // wait for Future to complete`
    futureResponse onComplete {
      case Success(response) => //println("Page details:" +response)
      //case Failure(error) => println("An error has occurred: " + error.getMessage)
    }
  }
  println("Get "+numPages+" Pages End")


  println("Get "+numPosts+" Post details Begin")
  Thread.sleep(sleepTime2)
  //GET POST REQ

  for (i <- 0 until numPosts) {
    Thread.sleep(sleepTime)
    val futureResponse = myClient.get("http://127.0.0.1:8080/getPost/"+i)
    // wait for Future to complete`
    futureResponse onComplete {
      case Success(response) => //println("The Post is "+response)
      //case Failure(error) => println("An error has occurred: " + error.getMessage)
    }
  }
  println("Get "+numPosts+" Post details End")

  //LIKE/POST PAGE REQ
  println("PageActivity - Random Likes and Posts by users for "+numPages+" Pages Begin")
  Thread.sleep(sleepTime2)
  for (i <- 0 until numPages) {

    Thread.sleep(sleepTime)
    val pipeline: HttpRequest => Future[String] = (
      sendReceive
        ~> unmarshal[String]
      )

    val Req: Future[String] = pipeline(Post("http://127.0.0.1:8080/Page/"+i))

    Req onComplete {
      case Success(response) => //println(response)
      //case Failure(error) => println("An error has occurred: " + error.getMessage)
    }
  }
  println("PageActivity - Random Likes and Posts by users for "+numPages+" Pages End")

  val totalSleepInReq = 3*sleepTime*numUsers+2*sleepTime*numPosts+2*sleepTime*numPages
  val totalSleepBetweenReq = 7*sleepTime2

  val totalTime = System.currentTimeMillis() - simBeginTime
  println("Simulation Completed!!! "+totalTime)
  val actualSimTime = totalTime - totalSleepInReq - totalSleepBetweenReq
  println("Processing Requests Time: " + actualSimTime + " milli seconds")
  val totalReqs = numUsers*4 + numPosts*2 + numPages*2 + numPages*1*2
  val throughPut = totalReqs*1000/actualSimTime
  println("Requests/sec: "+throughPut)
}

}


//var userFriends: Array[String] = new Array[String](500),
