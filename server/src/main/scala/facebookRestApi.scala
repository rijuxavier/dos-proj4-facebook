package main.scala

import akka.util.Timeout
import akka.actor._
import spray.http._
import HttpMethods._
import MediaTypes._
import spray.can.Http.RegisterChunkHandler
import akka.event.Logging
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout.durationToTimeout
import scala.concurrent.duration.DurationInt
import com.typesafe.config.ConfigFactory
import scala.concurrent.Future
import spray.http._
import scala.util.Random
import spray.can.Http
import spray.http.ContentTypes
import spray.http.HttpMethods.GET
import spray.http.HttpMethods.POST
import spray.http.HttpRequest
import spray.http.HttpResponse
import spray.json._
import spray.httpx.SprayJsonSupport

object numServer {
  val count: Int = 1 //TODO - Modify the numServer to same as Facebook server
}

object myJson extends DefaultJsonProtocol {
  implicit val userDataJson = jsonFormat4(userDataFormat)
  implicit val postDataJson = jsonFormat4(postDataFormat)
  implicit val pageDataJson = jsonFormat6(pageDataFormat)
}

case class userDataFormat(var userId: Long,
                          var userDname: String,
                          var userFriends: List[Int],
                          var userPostIds: List[Long]
                          )

case class postDataFormat(var postId: Long,
                          var postMessage: String,
                          var postCreatedTime: String,
                          var postToPage: Boolean
                         )

case class pageDataFormat(
                           var pageId: Long,
                           var pageLikes: Int,
                           var pageMembers: List[Long],
                           var pageAbout: String,
                           var pageDescription: String,
                           var pagePost: List[String]
                           //var pageAccessToken: Option[String],
                           //var pageCanPost: Option[Boolean],
                           //var pageCategory: Option[String],
                           //var pageContext: Option[String],
                           //var pageLocation: Option[String],
                           //var pageLink: Option[String],
                           //var pageName: Option[String]
                         )

object facebookHTTP {

  def main(args: Array[String]) {

    implicit val configSystem = ActorSystem("HTTPServer",
                 ConfigFactory.load(ConfigFactory.parseString(
                            """{ "akka" : { "actor" : { "provider" : "akka.remote.RemoteActorRefProvider" },
          	 								|"remote" |: { "enabled-transports" |: [ "akka.remote.netty.tcp" ],
          	 								|"netty" : { "tcp" : { "port" : 2552 , "maximum-frame-size" : 512000b } }
          	 				        |		} } } """.stripMargin)))

    val server = new Array[ActorSelection](numServer.count)

    for (i <- 0 until numServer.count) {
      server(i) = configSystem.actorSelection("akka.tcp://MasterActor@127.0.0.1:2552/user/Master" + i)

    }
    val apiHandler = configSystem.actorOf(Props(new facebookRestApi(server)), name = "handler")
    //bind our actor to an HTTP port
    IO(Http) ! Http.Bind(apiHandler, interface = "127.0.0.1", port = 8080)
  }
}


class facebookRestApi(server: Array[ActorSelection]) extends Actor with ActorLogging with AdditionalFormats with SprayJsonSupport {
  implicit val timeout: Timeout = 20.second

  import myJson._
  import context.dispatcher

  def receive = {

    case _: Http.Connected =>
      sender ! Http.Register(self)

//    //BUILD FB DB
//    case HttpRequest(POST, Uri.Path(path), _, _, _) if path startsWith "/build" =>
//
//      val sendTerminal = sender
//      println("Building Facebook")
//
//      val future: Future[String] = (server(0) ? MasterInit()).mapTo[String]
//
//      future.onSuccess {
//        case result: String =>
//          println("Building......." + result)
//          val body = HttpEntity(ContentTypes.`application/json`, result)
//          sendTerminal ! HttpResponse(entity = body)
//      }

    //ADD USER
    case HttpRequest(POST, Uri.Path(path), _, entity: HttpEntity.NonEmpty, _) if path startsWith "/addUser" =>

      val sendTerminal = sender
      val userId= path.split("/").last.toInt
      val body = entity.asString.asJson
      val value = body.convertTo[userDataFormat]
      //println("To add User: " + body + "value: " + value.userDname)

      val serverId = userId % numServer.count
      //println("Adding User: " +userId)

      val future: Future[String] = (server(serverId) ? NewUser(userId, value.userDname)).mapTo[String]

      future.onSuccess {
        case result: String =>
          println(result)
          val body = HttpEntity(ContentTypes.`application/json`, result)
          sendTerminal ! HttpResponse(entity = body)
      }

    //GET USER
    case HttpRequest(GET, Uri.Path(path), _, _, _) if path startsWith "/getUser" =>

      val sendTerminal = sender
      val userId = path.split("/").last.toInt
      val serverId = userId % numServer.count
      println("Fetch User: " + userId)
      val future: Future[userData] = (server(serverId) ? GetUserData(userId)).mapTo[userData]

      future.onSuccess {
        case result: userData =>
          val temp = new userDataFormat(result.userId,result.userDname,result.userFriends.toList,result.userPostIds.toList)
          val temp2 = temp.toJson.prettyPrint
          //println("Get userdata : " + temp2)
          val body = HttpEntity(ContentTypes.`application/json`, temp2)
          sendTerminal ! HttpResponse(entity = body)
      }

    //GET FRIENDS LIST
    case HttpRequest(GET, Uri.Path(path), _, _, _) if path startsWith "/getFriends" =>

      val sendTerminal = sender
      val userId = path.split("/").last.toInt
      val serverId = userId % numServer.count
      println("Fetch User: " + userId + " Friends")
      val future: Future[List[Int]] = (server(serverId) ? GetUserFriends(userId)).mapTo[List[Int]]

      future.onSuccess {
        case result: List[Int] =>
          val temp = result.toJson.toString
          //println("Get userFriends : " +temp)
          val temp2 = "Friends List: "+temp
        val body = HttpEntity(ContentTypes.`application/json`, temp2)
        sendTerminal ! HttpResponse(entity = body)
      }

    //POST REQ
    case HttpRequest(POST, Uri.Path(path), _, entity: HttpEntity.NonEmpty, _) if path startsWith "/addPost" =>

      val sendTerminal = sender
      val userId = path.split("/").last.toInt
      val serverId = userId%numServer.count
      val body = entity.asString.asJson
      val value = body.convertTo[postDataFormat]
      //println("FB Post to a User: "+userId+" "+tempdata)
      //println("FB Post to a User: " + body + "value: " + value.postMessage)

      val future: Future[String] = (server(serverId) ? PostReq(userId, value.postMessage)).mapTo[String]

      future.onSuccess {
        case result: String =>
          println(result)
          val body = HttpEntity(ContentTypes.`application/json`, result.toString)
          sendTerminal ! HttpResponse(entity = body)
      }

    //GET POST
    case HttpRequest(GET, Uri.Path(path), _, _, _) if path startsWith "/getPost" =>

      val sendTerminal = sender
      val userId = path.split("/").last.toInt
      val serverId = userId % numServer.count
      //println("Fetch Post: " + userId + " " + serverId)
      val future: Future[List[postData]] = (server(serverId) ? GetPost(userId)).mapTo[List[postData]]
      var temp3: String = ""
      future.onSuccess {
        case results: List[postData]  =>
          results.foreach(result => {
            val temp = new postDataFormat(result.postId,result.postMessage,result.postCreatedTime,result.postToPage)
            val temp2 = temp.toJson.prettyPrint
            temp3 = temp3+temp2
            //println("Get PostData : " +temp2)
            }
          )
          val body = HttpEntity(ContentTypes.`application/json`,temp3)
          sendTerminal ! HttpResponse(entity = body)
      }

    //ADD PAGE
    case HttpRequest(POST, Uri.Path(path), _, entity: HttpEntity.NonEmpty, _) if path startsWith "/addPage" =>

      val body = entity.asString.asJson
      val value = body.convertTo[pageDataFormat]

      val sendTerminal = sender
      val pageId = path.split("/").last.toInt
      val serverId = Random.nextInt(numServer.count)
      //val pageId = path.split("/").last.toInt
      //val serverId = pageId % numServer.count

      val future: Future[String] = (server(serverId) ? PageReq(pageId,value.pageAbout,value.pageDescription)).mapTo[String]

      future.onSuccess {
        case result: String =>
          //println("New Page added.." +result)
          val body = HttpEntity(ContentTypes.`application/json`, result.toString)
          sendTerminal ! HttpResponse(entity = body)
      }

    //GET PAGE DETAILS
    case HttpRequest(GET, Uri.Path(path), _, _, _) if path startsWith "/getPage" =>

      val sendTerminal = sender
      val pageId = path.split("/").last.toInt
      val serverId = pageId % numServer.count
      //println("Fetch Page: " + pageId + " " + serverId)
      val future: Future[pageData] = (server(serverId) ? GetPage(pageId)).mapTo[pageData]

      future.onSuccess {
        case result: pageData =>
          //println("Get pageData : " +result)
          val temp = new pageDataFormat(result.pageId,result.pageLikes,result.pageMembers,result.pageAbout,result.pageDescription,result.pagePost.toList)
          val temp2 = temp.toJson.prettyPrint
          val body = HttpEntity(ContentTypes.`application/json`, temp2)
          sendTerminal ! HttpResponse(entity = body)
      }

    //PAGE ACTIVITY
    case HttpRequest(POST, Uri.Path(path), _, _, _) if path startsWith "/Page" =>

      val sendTerminal = sender
      val pageId = path.split("/").last.toInt
      val serverId = pageId % numServer.count
      println("PageActivity: " + pageId + " " + serverId)
      val future: Future[String] = (server(serverId) ? PageActivity(pageId)).mapTo[String]

      future.onSuccess {
        case result: String =>
          //println("Get pageData : " +result)
          val body = HttpEntity(ContentTypes.`application/json`, result)
          sendTerminal ! HttpResponse(entity = body)
      }
    //***************************************************************//
  }
}


