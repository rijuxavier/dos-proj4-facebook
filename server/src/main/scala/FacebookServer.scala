package main.scala

import java.nio.ByteBuffer
import java.security.MessageDigest
import akka.actor._
import scala.collection.mutable.{ArrayBuffer, HashMap, ListBuffer}
import scala.language.postfixOps
import scala.util.Random
import java.util.Calendar

object Project4 extends App {

  var serverNode = new ArrayBuffer[ActorRef]
  var numServers: Int = 1  //TODO - Modify the numServer to same as HTTP server
  var sleepTime: Int = 0
  val system = ActorSystem("MasterActor")
  var x: Int = 0
  while (x < numServers) {
    var Act: ActorRef = system.actorOf(Props(new MasterActor(x)), name = "Master" +x)
    serverNode += Act
    x = x + 1
  }
  println(+x+" Servers created")

  def makeNewPostId(): Long = {
    var plainPostId = Random.nextInt(Int.MaxValue).toLong
    while (plainPostIds.contains(plainPostId)) {
      plainPostId = Random.nextInt(Int.MaxValue).toLong
    }
    plainPostIds += plainPostId
    return ByteBuffer.wrap(data.HashIt(plainPostId.toString)).getLong() >>> 32
  }

  def makeNewUserId(): Long = {
    var plainUserId = Random.nextInt(Int.MaxValue).toLong
    while (plainUserIds.contains(plainUserId)) {
      plainUserId = Random.nextInt(Int.MaxValue).toLong
    }
    plainUserIds += plainUserId
    return ByteBuffer.wrap(data.HashIt(plainUserId.toString)).getLong() >>> 32
  }

  def makeNewPageId(): Long = {
    var plainPageId = Random.nextInt(Int.MaxValue).toLong
    while (plainPageIds.contains(plainPageId)) {
      plainPageId = Random.nextInt(Int.MaxValue).toLong
    }
    plainPageIds += plainPageId
    return ByteBuffer.wrap(data.HashIt(plainPageId.toString)).getLong() >>> 32
  }

  def createUser(id: Int,userId: Long,userData: String): Unit = {
    val noFriends: ListBuffer[Int] = new ListBuffer[Int]()
    val noPostIds: ListBuffer[Long] = new ListBuffer[Long]()
    val User = new userData(0,"",noFriends,noPostIds)
    //val tempId = makeNewUserId()
    User.userId = userId
    User.userDname = userData
    User.userFriends = noFriends
    data.userList.put(id,User)
  }

  def createPost(id: Int,postId: Long,inpData: String): Unit = {
    //val tempdata = "Hi! from User"+id
    val tempDate = Calendar.getInstance().getTime()
    val tempPost = postData(postId,inpData,tempDate.toString,false)
    data.post.put(postId,tempPost)
  }

  def createPagePost(id: Int,postId: Long,inpData: String): Unit = {
    //val tempdata = "Hi! from User"+id
    val tempDate = Calendar.getInstance().getTime()
    val tempPost = postData(postId,inpData,tempDate.toString,true)
    data.post.put(postId,tempPost)
  }

  def createPage(id: Int,pageId: Long,pageAbt: String,pageDesc: String): Unit = {
    val tempPagePosts: ListBuffer[String] = new ListBuffer[String]()
    val tempPage = pageData(pageId,0,Nil,pageAbt,pageDesc,tempPagePosts)
    data.page.put(id,tempPage)
  }

  var countServer: Int = 0
  var randServer: Int = 0
  var StartTime: Long = 0
  var printCount: Int = 0
  var friendsCounter: Int = 0
  var postsCounter: Int = 0
  var countUsers: Int = 0
  var usersInEachServer: Int = 0
  var serverId: Int = 0
  var randUserId: Int = 0
  var Master: ActorRef = _
  var randUser: Int = 0

  var plainPostIds: ListBuffer[Long] = new ListBuffer[Long]()
  var plainUserIds: ListBuffer[Long] = new ListBuffer[Long]()
  var plainPageIds: ListBuffer[Long] = new ListBuffer[Long]()
  var userName: String = _

  class MasterActor(masterId: Int) extends Actor with ActorLogging {

    def receive = {

//      case BuildFacebook() =>
//        for (i <- 0 until data.totalUsers) {
//          //self ! NewUser(i,data)
//        }

      case NewUser(id,newUserData) =>
        println("In newUser"+id)
        val newUserId = makeNewUserId()
        createUser(id,newUserId,newUserData)
        //val fetchId = data.userList.get(id)
        self ! FriendReq(id)
        self ! PostUserReq(id)
        Thread.sleep(sleepTime)
        sender ! "facebook user:"+id+" is now created with unique id:"+newUserId.toString

      case GetUserData(id) =>
        println("In getuser details"+id)
        val temp = data.userList.get(id)
        //println(temp.get)
        if (temp.isEmpty) {
          sender ! ""
        }
        else {
          sender ! temp.get
        }

      case GetUserFriends(id) =>
        println("In get userFriends:"+id)
        val tempFriends = data.userList.get(id)
//        if (tempFriends.get.userFriends == "") {
//          sender ! "No friends for this user"
//        }
//        else{
//        println(tempFriends.get.userFriends)
        sender ! tempFriends.get.userFriends.toList

      case GetPost(id) =>
        Thread.sleep(sleepTime)
        val temp = data.userList.get(id)
        var postLB: ListBuffer[postData] = new ListBuffer[postData]()
        if(temp.isEmpty) {
          sender! postLB.toList
        }
        else {
          var loop = temp.get.userPostIds.length
          println("total no. of posts "+loop)
          if (loop > 30)
            {
              loop = 30
            }
          for (i <- 0 until loop) {
            val tempPostId = temp.get.userPostIds(i)
            val tempdata = data.post.get(tempPostId)
            postLB += tempdata.get
          }
          //println("In GetPost:"+id+" "+postLB)
          sender ! postLB.toList
        }

      case GetPage(id) =>
        Thread.sleep(sleepTime)
        val tempPage = data.page(id)
        println("In get page:"+id)
        sender ! tempPage

      case FriendReq(id) =>
        //println("Before:"+data.userList)
        //send a friend request to another user in entire system
        var randUser = Random.nextInt(plainUserIds.length)
        while (randUser == id) randUser = Random.nextInt(plainUserIds.length)
        //printf("i: %s randUser: %s \n", i, randUser)
        val temp = data.userList.get(id)

        if(temp.isEmpty || !temp.get.userFriends.contains(randUser.toString)) {
          //append the new random user as my friend.
          temp.get.userFriends+= randUser
          //send msg to new friend to append me as friend after finding the server where the new friend belongs
          val server = randUser%numServers
          //printf("\nrandomFriend: %s and serverId: %s \n",randUser,server)
          //data.allServerRefs(server) ! UpdateFriend(randUser,i)
          serverNode(server) ! UpdateFriend(randUser,id)
        }

      case UpdateFriend(myid,myfriend) =>
        val temp = data.userList.get(myid)
        Thread.sleep(sleepTime)
        //println("In update friend")
        //      if(temp.get.userFriends.contains(myfriend.toString)) {
        //        //println("myid"+myid+"already in mylist"+myfriend)
        //      }
        if (temp.isEmpty || !temp.get.userFriends.contains(myfriend.toString)) {
          temp.get.userFriends+= myfriend
        }
        //printf("myfriend: %s myid: %s \n",myfriend,myid)

      case PostReq(id,inpData) =>
        //for (i <- lowLim until UppLim) {
        Thread.sleep(sleepTime)
        val temp = data.userList.get(id)
        //println("In make post"+id)
        //send a random post to one of my friend
        val seed = temp.get.userFriends.length
        val index = Random.nextInt(seed)
        val randFriendId = temp.get.userFriends(index)
        val friendServerId = randFriendId%numServers
        //printf("my userlist: %s index: %s randFriend: %s\n",temp.get.userFriends,index,randFriendId)
        //printf("Makepost - myid: %s friend'id: %s and his server: %d\n",id,randFriendId,friendServerId)
        val newPostId = makeNewPostId()
        temp.get.userPostIds += newPostId
        createPost(id,newPostId,inpData)
        //data.allServerRefs(friendServerId) ! UpdatePost(newPostId,randFriendId)
        serverNode(friendServerId)  ! UpdatePost(newPostId,randFriendId)
        sender ! "New post created with id: "+newPostId.toString
        //}

      case UpdatePost(newPostId,friendId) =>
        //println("Updatepost: myid in server:"+friendId+" "+serverId)
        val temp = data.userList.get(friendId)
        temp.get.userPostIds += newPostId

      case PageReq(id,pageAbt,pageDesc) =>
        println("in Create Page Req")
        val newPageId = makeNewPageId()
        createPage(id,newPageId,pageAbt,pageDesc)
        sender ! "New page created"+newPageId


      case PageActivity(id) =>

        val pageActivityData = data.page.get(id)
        val numUsers = data.userList.size
        println("In page activity:"+id)
        for (i <- 0 until numServers){
          //random user is going to like and post to a page//
          val newPostId = makeNewPostId()
          val randUserId = Random.nextInt(numUsers)
          val randUserServerId = randUserId%numServers
          serverNode(randUserServerId)  ! UpdatePost(newPostId,randUserId)
          val newPostMsg = "Post Message from User:"+randUserId
          pageActivityData.get.pagePost+= newPostMsg
          createPagePost(id,newPostId,newPostMsg)
          pageActivityData.get.pageLikes = pageActivityData.get.pageLikes+1

          val pageActivityUserData = data.userList.get(randUserId)
          pageActivityUserData.get.userPostIds+= newPostId
          //var tempLikes = tempPage.get.pageLikes
          //tempPage.get.pageLikes = tempLikes
          //printf("\nNew PagePost: %s pageLikes: %s \n",tempPage.get.pagePost,tempPage.get.pageLikes)
        }

        sender ! "Page activity completed"

//      case PrintAll() =>
//        println("server:"+serverNode+" "+data.userList)
//
//      case AllDone() =>
//        printCount += 1
//        if (printCount == data.servers) {
//          println("All posts" + data.post)
//          println("All Good!")
//          println("Time taken: " + (System.currentTimeMillis() - StartTime) + " milli seconds")
//          //context.system.shutdown()
        }
    }
  }



case class NewUser(id: Int, data: String) extends Project4messages
case class FriendReq(id: Int) extends  Project4messages
case class UpdateFriend(myId: Int,myFriend: Int) extends Project4messages
case class GetUserData(id: Int) extends Project4messages
case class GetUserFriends(id: Int) extends Project4messages

case class PostUserReq(id: Int) extends Project4messages
case class PostReq(id: Int,inpData: String) extends Project4messages
case class GetPost(id: Int) extends Project4messages
case class UpdatePost(postId: Long, friendId: Int) extends Project4messages

case class PageReq(id: Int,pageAbout: String,pageDesc: String) extends Project4messages
case class GetPage(id: Int) extends Project4messages
case class PageActivity(id: Int) extends Project4messages

//case class BuildFacebook() extends Project4messages
//case class AllDone() extends  Project4messages
//case class PrintAll() extends Project4messages
//case class PrintUserLists() extends Project4messages

sealed trait Project4messages

object data {

  var post = new HashMap[Long,postData]()
  var page = new HashMap[Long,pageData]()
  var userList = new HashMap[Int,userData]()

  def HashIt(s: String): Array[Byte] = {
    return MessageDigest.getInstance("MD5").digest(s.getBytes("UTF-8"))

  //var totalUsers: Int = 50
  //var usersPerServer: Int = 10
  //var servers: Int = totalUsers/usersPerServer
  //var servers: Int = 1
  //var allServerRefs: ListBuffer[ActorRef] = new ListBuffer[ActorRef]()
  //var loop: Int = 0

  }
}

//Facebook API classes//
case class userData(var userId: Long,
                          var userDname: String,
                          var userFriends: ListBuffer[Int],
                          var userPostIds: ListBuffer[Long]
                   )

case class postData(var postId: Long,
                          var postMessage: String,
                          var postCreatedTime: String,
                          var postToPage: Boolean
                         )

case class pageData(
                     var pageId: Long,
                     var pageLikes: Int,
                     var pageMembers: List[Long],
                     var pageAbout: String,
                     var pageDescription: String,
                     var pagePost: ListBuffer[String]
                           //var pageAccessToken: Option[String],
                           //var pageCanPost: Option[Boolean],
                           //var pageCategory: Option[String],
                           //var pageContext: Option[String],
                           //var pageLocation: Option[String],
                           //var pageLink: Option[String],
                           //var pageName: Option[String]
                         )
